# text emotion detection using Bert on Docker

This code file contains a model that is not presented in the dissertation as it is sligthly advanced and part of the testing done and not completed. There are not a lot of commens but if you study the previous simpler models you can change or copy-paste the code, and create you are own file to run the relevant experiments with Bert on Docker.
The model presented here is inspired by the paper Dynamic Meta-Embeddings for Improved Sentence Representations https://www.aclweb.org/anthology/D18-1176.pdf. It has as its main components the BiGRU, SASE and Bert classifier model.

I also provide the pod for the Kubernetes container and the Dockerfile used to create the image used in the pod file.
The main things to worry is the path_to/text_emotion_classification_ensemble_model.py and the path_to/data.
data is a directory that contains all three of the datasets used, the weights of trained BiGRU and SASE and the glove word embeddings. In this [link](https://gitlab.com/Georgios.Hadjiharalambous/emotion-detection-in-short-texts-datasets) are all of the above provided for ease. Moreover, data contains a folder called bert which containes the model of Bert and the Bert config file. These files can be found here bert config file: https://s3.amazonaws.com/models.huggingface.co/bert/bert-base-uncased-config.json and bert model binary : https://cdn.huggingface.co/bert-base-uncased-pytorch_model.bin .

The final structure of the directories should look like:

-data/<br/>
----your_python_code.py<br/>
----TEC.csv<br/>
----Wang.csv<br/>
----CrowdFlower.csv<br/>
----glove.6B.300d.txt<br/>
----glove.6B.50d.txt<br/>
----other files necessary for your purposes e.g lexicons if using Bert Lex<br/>
----bert/<br/>
-------bert_model.bin<br/>
-------bert_config.json<br/>