
# Imports and settings.

import torch
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import warnings
from torch.autograd import Variable
from sklearn.metrics import precision_score
from sklearn.metrics import recall_score
from sklearn.metrics import accuracy_score
from sklearn.metrics import fbeta_score
from sklearn.metrics import confusion_matrix
from sklearn.metrics import classification_report
from sklearn.exceptions import UndefinedMetricWarning
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from tensorflow import keras
import tensorflow as tf
from keras.models import Model

from transformers import BertTokenizer
from torch.utils.data import TensorDataset, DataLoader, RandomSampler, SequentialSampler
from transformers import BertForSequenceClassification, AdamW, BertConfig,BertModel
from transformers import get_linear_schedule_with_warmup
print('libraries installed')
warnings.filterwarnings("ignore", category=UndefinedMetricWarning)
import sys
if (len(sys.argv)<2):
    print('data base path not provided as argument please exit')
    exit()
base_path=sys.argv[1]



sase_path_cf=base_path+'cf_sase_multiclass.pth'
bigru_path_cf=base_path+'cf_bigru_multiclass.pth'
sase_path_tec=base_path+'tec_sase_multiclass.pth'
bigru_path_tec=base_path+'tec_bigru_multiclass.pth'
sase_path_wang=base_path+'wang_sase_multiclass.pth'
bigru_path_wang=base_path+'wang_bigru_multiclass.pth'



class SelfAttentiveSentenceEmbeddingModel(torch.nn.Module):

    # This class is the implementation of the SASE model (Lin et al. 2017).
    # The code is based on an existing PyTorch implementation of the SASE model available at: https://github.com/kaushalshetty/Structured-Self-Attention.

    def __init__(self, batch_size, lstm_hidden_dimension, dense_hidden_dimension, num_attention_hops, num_timesteps, pretrained_embedding,
                 embedding_dimension, multiclass, num_classes):

        # Initializes the SASE model object.

        # Parameters:
        #     batch_size: {int} Batch size used in model training.
        #     lstm_hidden_dimension: {int} Hidden dimension for the bidirectional LSTM layer.
        #     dense_hidden_dimension: {int} Hidden dimension for the dense layer.
        #     num_attention_hops: {int} Number of attention hops.
        #     num_timesteps: {int} Number of LSTM timesteps.
        #     pretrained_embedding: {torch.Tensor} Pretrained embedding to be loaded.
        #     embedding_dimension: {int} Embedding dimension.
        #     multiclass: {bool} Use multiclass or binary classification.
        #     num_classes: {int} Number of classes.

        super(SelfAttentiveSentenceEmbeddingModel, self).__init__()
        self.batch_size = batch_size
        self.lstm_hidden_dimension = lstm_hidden_dimension
        self.dense_hidden_dimension = dense_hidden_dimension
        self.num_attention_hops = num_attention_hops
        self.num_timesteps = num_timesteps
        self.multiclass = multiclass
        self.num_classes = num_classes
        self.embedding, self.embedding_dimension = self.load_embedding(pretrained_embedding)
        self.lstm = torch.nn.LSTM(self.embedding_dimension, self.lstm_hidden_dimension, 1, batch_first=True, bidirectional=True)
        self.linear_first = torch.nn.Linear(self.lstm_hidden_dimension * 2, self.dense_hidden_dimension * 2)
        self.linear_first.bias.data.fill_(0)
        self.linear_second = torch.nn.Linear(self.dense_hidden_dimension * 2, self.num_attention_hops)
        self.linear_second.bias.data.fill_(0)
        self.linear_final = torch.nn.Linear(self.lstm_hidden_dimension * 2, self.num_classes)
        self.hidden_state = self.init_hidden()

    def load_embedding(self, pretrained_embedding):

        # Loads the pretrained embedding.

        # Parameters:
        #     pretrained_embedding: {torch.Tensor} Pretrained embedding to be loaded.

        embedding = torch.nn.Embedding(pretrained_embedding.size(0), pretrained_embedding.size(1))
        embedding.weight = torch.nn.Parameter(pretrained_embedding)
        embedding_dimension = pretrained_embedding.size(1)
        return embedding, embedding_dimension

    def softmax(self, tensor, axis):

        # Applies softmax to a selected axis of the provided tensor.

        # Parameters:
        #     tensor: {torch.Tensor} Tensor to which softmax is to be applied.
        #     axis: {int} Axis to which softmax is to be applied.

        tensor_size = tensor.size()
        transposed_tensor = tensor.transpose(axis, len(tensor_size) - 1)
        transposed_size = transposed_tensor.size()
        tensor_2d = transposed_tensor.contiguous().view(-1, transposed_size[-1])
        softmax_2d = torch.nn.functional.softmax(tensor_2d, 1)
        softmax_nd = softmax_2d.view(*transposed_size)
        return softmax_nd.transpose(axis, len(tensor_size) - 1)

    def init_hidden(self):

        # Initializes the hidden state.
        if torch.cuda.is_available():
        	device = torch.device("cuda")
        else:
        	device = torch.device("cpu")

        return (Variable(torch.zeros(2, self.batch_size, self.lstm_hidden_dimension).to(device)),
                Variable(torch.zeros(2, self.batch_size, self.lstm_hidden_dimension).to(device)))

    def forward(self, input_data):

        # Processes the input data to produce the classification output and the attention weight tensor.

        # Parameters:
        #     input_data: {torch.Tensor} Input data.

        embedding = self.embedding(input_data)
        result, self.hidden_state = self.lstm(embedding.view(self.batch_size, self.num_timesteps, -1), self.hidden_state)
        input_tensor = torch.tanh(self.linear_first(result))
        input_tensor = self.linear_second(input_tensor)
        input_tensor = self.softmax(input_tensor, 1)
        attention = input_tensor.transpose(1, 2)
        sentence_embedding = attention@result
        average_embedding = torch.sum(sentence_embedding, 1) / self.num_attention_hops
        if not self.multiclass:
            output = torch.sigmoid(self.linear_final(average_embedding))
        else:
            output = torch.log_softmax(self.linear_final(average_embedding), 1)
        return output, attention,average_embedding

    def frobenius_norm(self, matrix):

        # Calculates the Frobenius norm of the provided matrix.

        # Parameters:
        #     matrix: {torch.Tensor} Matrix for which the Frobenius norm is to be calculated.

        return torch.sum(torch.sum(torch.sum(matrix ** 2, 1), 1) ** 0.5).type(torch.DoubleTensor)









class bigru(torch.nn.Module):
    def __init__(self,batch_size,embedding, embedding_dimension, max_length, gru_hidden_dimension, num_classes):
      super(bigru, self).__init__()
      self.batch_size=batch_size
      self.max_length=max_length
      self.gru_hidden_dimension=gru_hidden_dimension
      self.embedding, self.embedding_dimension = self.load_embedding(embedding)
      self.hidden_state = self.init_hidden()
      self.bidirectional_gru = torch.nn.GRU(self.embedding_dimension, self.gru_hidden_dimension, 1, batch_first=True, bidirectional=True)
      self.linear = torch.nn.Linear(4 * self.gru_hidden_dimension ,2 * max_length)#?????????????????????????????????
      self.linear_final = torch.nn.Linear(2*max_length,num_classes)
      self.dropout=torch.nn.Dropout(p=0.5)
      self.relu = torch.nn.ReLU()
      #  model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])

    def init_hidden(self):

        # Initializes the hidden state.
        if torch.cuda.is_available():
        	device = torch.device("cuda")
        else:
        	device = torch.device("cpu")
        return Variable(torch.zeros(2, self.batch_size, self.gru_hidden_dimension).to(device))


    def load_embedding(self, pretrained_embedding):

        # Loads the pretrained embedding.

        # Parameters:
        #     pretrained_embedding: {torch.Tensor} Pretrained embedding to be loaded.

        embedding = torch.nn.Embedding(pretrained_embedding.size(0), pretrained_embedding.size(1))
        embedding.weight = torch.nn.Parameter(pretrained_embedding)
        embedding_dimension = pretrained_embedding.size(1)
        return embedding, embedding_dimension

    def forward(self,input_data):
      embedding=self.embedding(input_data)
      bi_gru_result , self.hidden_state = self.bidirectional_gru(embedding,self.hidden_state) #self.bidirectional_gru(embedding.view(self.batch_size, self.max_length, -1), self.hidden_state)
      avg_pool = torch.mean(bi_gru_result, 1)
      max_pool, _ = torch.max(bi_gru_result, 1)
      concat = torch.cat((avg_pool,max_pool),1)
      lin_res = self.relu(self.linear(concat))
      drop = self.dropout(lin_res)
      output_emb = self.linear_final(drop)
      output = torch.log_softmax(output_emb,1)
      return output, drop

#Dynamic Meta-Embeddings for Improved Sentence Representations https://www.aclweb.org/anthology/D18-1176.pdf

class dme(torch.nn.Module):
    def __init__(self, num_classes, bigru, sase,projection_dim=80):
        super(dme, self).__init__()
        bigru_len=70
        sase_len=100
        bert_len=768



        self.lin_projection_bigru = torch.nn.Linear(bigru_len,projection_dim)
        self.lin_projection_sase = torch.nn.Linear(sase_len,projection_dim)
        self.lin_projection_bert = torch.nn.Linear(bert_len,projection_dim)

        self.attend_bert = torch.nn.Linear(projection_dim,1)
        self.attend_bigru = torch.nn.Linear(projection_dim,1)
        self.attend_sase = torch.nn.Linear(projection_dim,1)

        self.final_layer = torch.nn.Linear(projection_dim * 1, num_classes)

        self.sase = sase
        self.bigru = bigru

        self.bert = BertModel.from_pretrained(base_path+'bert/')
        self.dropout = torch.nn.Dropout(p=0.5)


    def forward(self, x, input_ids, attention_mask):
        x_bert = self.bert(input_ids, attention_mask = attention_mask,)[1]
        _ , _, x_sase = self.sase(x)
        _ , x_bigru = self.bigru(x)

        #projected to same dimension space
        bigru_proj = self.lin_projection_bigru(x_bigru)
        sase_proj = self.lin_projection_sase(x_sase)
        bert_proj=self.lin_projection_bert(x_bert)

        #find attention weights
        bert_attention = torch.sigmoid(self.attend_bert(bert_proj))
        bigru_attention = torch.sigmoid(self.attend_bigru(bigru_proj))
        sase_attention = torch.sigmoid(self.attend_sase(sase_proj))

        #calculate weigheted sum and add
        x = bigru_attention * bigru_proj + sase_attention * sase_proj+ bert_attention * bert_proj
        #x_con = torch.cat((bigru_attention * bigru_proj , sase_attention * sase_proj),dim=1)

        return torch.log_softmax(self.final_layer(x),1)

    def set_batch_size(self,num):
        self.sase.batch_size=num
        self.bigru.batch_size=num

    def set_hidden_state(self,num):
      self.set_batch_size(num)
      self.sase.hidden_state = self.sase.init_hidden()
      self.bigru.hidden_state = self.bigru.init_hidden()

class comb(torch.nn.Module):
    def __init__(self, num_classes, bigru, sase):
        super(comb, self).__init__()
        bigru_len=70
        sase_len=100
        self.lin = torch.nn.Linear(sase_len+bigru_len,80)#70->bigru , 100 ->sase
        self.lin2 = torch.nn.Linear(80,num_classes)
        self.lin3 = torch.nn.Linear(400,num_classes)
        self.sase = sase
        self.bigru = bigru
        self.dropout = torch.nn.Dropout(p=0.5)


    def forward(self, x):
        _ , _, x_sase = self.sase(x)
        xbigru, x_bigru = self.bigru(x)
        x_comb = torch.cat((x_sase,x_bigru),dim=1)



        lin1_out=self.lin(x_comb)
        drop = self.dropout(lin1_out)
        return torch.log_softmax(self.lin2(lin1_out),1)

    def set_batch_size(self,num):
        self.sase.batch_size=num
        self.bigru.batch_size=num

    def set_hidden_state(self,num):
      self.set_batch_size(num)
      self.sase.hidden_state = self.sase.init_hidden()
      self.bigru.hidden_state = self.bigru.init_hidden()

def get_classes_stats(predicted_output, true_output):

    # Returns the calculated evaluation metrics for each class.

    # Parameters:
    #     predicted_output: {numpy.array} Predicted output.
    #     true_output: {numpy.array} True output.

    report_dict = classification_report(true_output, predicted_output, output_dict=True)
    classes = []
    classes_stats = []
    for key in report_dict:
        if key not in ["accuracy", "macro avg", "weighted avg"]:
            classes.append(int(float(key)))
            stats = [float("%0.3f" % (report_dict[key]['precision'])), float("%0.3f" % (report_dict[key]['recall'])),
                     float("%0.3f" % (report_dict[key]['f1-score'])), report_dict[key]['support']]
            classes_stats.append(stats)
    return [classes, classes_stats]

def plot_confusion_matrix(description, predicted_output, true_output, labels_dict, normalize=False):

    # Plots a confusion matrix for the predicted output.

    # Parameters:
    #     description: {str} Description.
    #     predicted_output: {numpy.array} Predicted output.
    #     true_output: {numpy.array} True output.
    #     labels_dict: {dict} Dictionary of class labels.
    #     normalize: {bool} Normalize values or not.

    title = "Confusion matrix: '%s'" % (description)
    if normalize:
        title = title + " (Normalized)"
    cm = confusion_matrix(true_output, predicted_output)
    classes = get_classes_stats(predicted_output, true_output)[0]
    labels = []
    for item in classes:
        labels.append(labels_dict[item])
    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
    fig = plt.figure(figsize=(12, 12))
    ax = fig.add_subplot(1, 1, 1)
    cmap = plt.cm.Blues
    im = ax.imshow(cm, interpolation='nearest', cmap=cmap)
    ax.figure.colorbar(im, ax=ax)
    ax.set(xticks=np.arange(cm.shape[1]),
           yticks=np.arange(cm.shape[0]), yticklabels=labels)
    ax.set_xticklabels(labels, rotation='vertical')
    ax.set_title(title, fontsize=20)
    ax.set_ylabel("True labels", fontsize=20)
    ax.set_xlabel("Predicted labels", fontsize=20)
    fmt = '.2f' if normalize else 'd'
    thresh = cm.max() / 2.
    for i in range(cm.shape[0]):
        for j in range(cm.shape[1]):
            ax.text(j, i, format(cm[i, j], fmt), ha="center", va="center", color="white" if cm[i, j] > thresh else "black")
    ax.grid(False)

def evaluation_summary(description, predicted_output, true_output, labels_dict, verbose=True):

    # Returns an evaluation report for the predicted output.

    # Parameters:
    #     description: {str} Description.
    #     predicted_output: {numpy.array} Predicted output.
    #     true_output: {numpy.array} True output.
    #     labels_dict: {dict} Dictionary of class labels.
    #     verbose: {bool} Verbose or not.

    precision = precision_score(true_output, predicted_output, average='weighted')
    recall = recall_score(true_output, predicted_output, average='weighted')
    accuracy = accuracy_score(true_output, predicted_output)
    f1 = fbeta_score(true_output, predicted_output, 1, average='weighted')
    classes = get_classes_stats(predicted_output, true_output)[0]
    classes_stats = get_classes_stats(predicted_output, true_output)[1]
    result = [[accuracy, precision, recall, f1]]
    if verbose:
        print("Classifier '%s':" % (description))
        print("Accuracy: %0.3f" % (accuracy))
        print("Precision (WEIGHT_AVG): %0.3f" % (precision))
        print("Recall (WEIGHT_AVG): %0.3f" % (recall))
        print("F1 (WEIGHT_AVG): %0.3f" % (f1))
    for i in range(len(classes)):
        if verbose:
            print((labels_dict[classes[i]] + " | Precision: %0.3f Recall: %0.3f F1: %0.3f Support: " + str(classes_stats[i][3])) %
                  (classes_stats[i][0], classes_stats[i][1], classes_stats[i][2]))
        result.append([labels_dict[classes[i]], classes_stats[i][0], classes_stats[i][1], classes_stats[i][2]])
    return result

def create_report(parameters, run_function, num_classes, num_iters):

    # Creates an overall evaluation report after training and testing a model on binary or multiclass classification for a set number of iterations.

    # Parameters:
    #     parameters: {list} List of parameters.
    #     run_function: {function} Function to train and test a model on binary or multiclass classification.
    #     num_classes: {int} Number of classes.
    #     num_iters: {int} Number of iterations.

    accuracy_list = []
    precision_list = []
    recall_list = []
    f1_list = []
    classes = {}
    print("CREATING REPORT:")
    for i in range(num_iters):
        print("Run: " + str(i + 1))
        result,_ = run_function(parameters[0], parameters[1], parameters[2], parameters[3], False)
        print("Accuracy: %0.3f" % result[0][0])
        print("Precision (WEIGHT_AVG): %0.3f" % result[0][1])
        print("Recall (WEIGHT_AVG): %0.3f" % result[0][2])
        print("F1 (WEIGHT_AVG): %0.3f" % result[0][3])
        accuracy_list.append(result[0][0])
        precision_list.append(result[0][1])
        recall_list.append(result[0][2])
        f1_list.append(result[0][3])
        for j in range(1, num_classes + 1, 1):
            print((str(result[j][0]) + " | Precision: %0.3f Recall: %0.3f F1: %0.3f") % (result[j][1], result[j][2], result[j][3]))
            if result[j][0] not in classes:
                classes[result[j][0]] = {}
            if "Precision_List" not in classes[result[j][0]]:
                classes[result[j][0]]["Precision_List"] = []
            classes[result[j][0]]["Precision_List"].append(result[j][1])
            if "Recall_List" not in classes[result[j][0]]:
                classes[result[j][0]]["Recall_List"] = []
            classes[result[j][0]]["Recall_List"].append(result[j][2])
            if "F1_List" not in classes[result[j][0]]:
                classes[result[j][0]]["F1_List"] = []
            classes[result[j][0]]["F1_List"].append(result[j][3])
    general_stats = [["%0.3f [%0.3f]" % (np.mean(np.asarray(accuracy_list)), np.std(np.asarray(accuracy_list))),
                      "%0.3f [%0.3f]" % (np.mean(np.asarray(precision_list)), np.std(np.asarray(precision_list))),
                      "%0.3f [%0.3f]" % (np.mean(np.asarray(recall_list)), np.std(np.asarray(recall_list))),
                      "%0.3f [%0.3f]" % (np.mean(np.asarray(f1_list)), np.std(np.asarray(f1_list)))]]
    class_labels = []
    class_stats = []
    for key in classes:
        class_labels.append(key)
        precision = "%0.3f [%0.3f]" % (np.mean(np.asarray(classes[key]["Precision_List"])), np.std(np.asarray(classes[key]["Precision_List"])))
        recall = "%0.3f [%0.3f]" % (np.mean(np.asarray(classes[key]["Recall_List"])), np.std(np.asarray(classes[key]["Recall_List"])))
        f1 = "%0.3f [%0.3f]" % (np.mean(np.asarray(classes[key]["F1_List"])), np.std(np.asarray(classes[key]["F1_List"])))
        class_stats.append([precision, recall, f1])
    fig = plt.figure()
    ax = fig.add_subplot(3, 1, 1)
    col_labels = ["Accuracy [STD]", "Precision (WEIGHT_AVG) [STD]", "Recall (WEIGHT_AVG) [STD]", "F1 (WEIGHT_AVG) [STD]"]
    table = ax.table(cellText=general_stats, rowLabels=[parameters[0]], colLabels=col_labels, loc='center')
    table.scale(3, 3)
    ax.axis('off')
    ax = fig.add_subplot(3, 1, 3)
    col_labels = ["Precision [STD]", "Recall [STD]", "F1 [STD]"]
    table = ax.table(cellText=class_stats, rowLabels=class_labels, colLabels=col_labels, loc='center')
    table.scale(3, 3)
    ax.axis('off')

def load_dataset(load_function, max_features, max_length,batch_size, target=None):

    # Processes a dataset for the training and testing of the BiGRU model.

    # Parameters:
    #     load_function: {function} Function to load a dataset.
    #     max_features: {int} Maximal size of the vocabulary.
    #     max_length: {int} Maximal length of a dataset entry in words.
    #     target: {str} Target class.

    if target == None:
        train_set,  test_set, labels_dict = load_function()
    else:
        train_set,  test_set, labels_dict = load_function(target)
    train_input, train_output = train_set[0], train_set[1]
    test_input, test_output = test_set[0], test_set[1]
    #BERT data
    bert_train_input, bert_train_masks = tokenize_map_pad_mask(train_input, max_length)
    bert_train_input_np = np.array(bert_train_input, dtype=np.float32)
    bert_train_masks_np = np.array(bert_train_masks, dtype=np.float32)

    bert_test_input, bert_test_masks= tokenize_map_pad_mask(test_input, max_length)
    bert_test_input_np = np.array(bert_test_input, dtype=np.float32)
    bert_test_masks_np = np.array(bert_test_masks, dtype=np.float32)


    train_input_list = train_input.tolist()
    test_input_list = test_input.tolist()
    tokenizer = Tokenizer(num_words=max_features)
    tokenizer.fit_on_texts(train_input_list)
    encoded_train_input = tokenizer.texts_to_sequences(train_input_list)
    encoded_test_input = tokenizer.texts_to_sequences(test_input_list)
    word_to_id = tokenizer.word_index
    train_input_pad = pad_sequences(encoded_train_input, maxlen=max_length)
    test_input_pad = pad_sequences(encoded_test_input, maxlen=max_length)


    train_input_np = np.array(train_input_pad, dtype=np.float32)
    test_input_np = np.array(test_input_pad, dtype=np.float32)
    train_output_np = np.array(train_output, dtype=np.float32)
    test_output_np = np.array(test_output, dtype=np.float32)

    train_data = torch.utils.data.TensorDataset(torch.from_numpy(train_input_np).type(torch.LongTensor),
                                                    torch.from_numpy(train_output_np).type(torch.LongTensor),
                                                    torch.from_numpy(bert_train_input_np).type(torch.LongTensor),
                                                    torch.from_numpy(bert_train_masks_np).type(torch.LongTensor))

    train_loader = torch.utils.data.DataLoader(train_data, batch_size=batch_size, drop_last=True)


    return train_loader,  test_input_np, bert_test_input_np, bert_test_masks_np, test_output_np, word_to_id, labels_dict


def load_crowdflower_multiclass():

    # Loads the CrowdFlower dataset for multiclass classification with the BiGRU model.

    labels_dict = {0: "Anger", 1: "Fear", 2: "Sadness", 3: "Happiness"}
    dataframe = pd.read_csv(base_path+"CrowdFlower.csv", delimiter=",", dtype={"id": int, "emotion": str, "author": str, "text": str})
    dataframe = dataframe.drop(dataframe.columns[[0, 2]], axis=1)
    dataframe = dataframe[dataframe.emotion != "empty"]
    dataframe = dataframe[dataframe.emotion != "love"]
    dataframe = dataframe[dataframe.emotion != "fun"]
    dataframe = dataframe[dataframe.emotion != "relief"]
    dataframe = dataframe[dataframe.emotion != "enthusiasm"]
    dataframe = dataframe[dataframe.emotion != "boredom"]
    dataframe = dataframe[dataframe.emotion != "anger"]
    dataframe = dataframe[dataframe.emotion != "neutral"]
    dataframe = dataframe[dataframe.emotion != "surprise"]
    dataframe["emotion"] = dataframe["emotion"].replace("hate", 0).replace("worry", 1)
    dataframe["emotion"] = dataframe["emotion"].replace("sadness", 2).replace("happiness", 3)
    dataframe = dataframe.sample(frac=1).reset_index(drop=True)
    min_size = 1310
    dataframe_anger = dataframe[dataframe.emotion == 0][:min_size]
    dataframe_fear = dataframe[dataframe.emotion == 1][:min_size]
    dataframe_sadness = dataframe[dataframe.emotion == 2][:min_size]
    dataframe_happiness = dataframe[dataframe.emotion == 3][:min_size]
    dataframe_anger_train = dataframe_anger[:int(0.8 * min_size)]
#    dataframe_anger_val = dataframe_anger[int(0.6 * min_size):int(0.8 * min_size)]
    dataframe_anger_test = dataframe_anger[int(0.8 * min_size):]
    dataframe_fear_train = dataframe_fear[:int(0.8 * min_size)]
#   dataframe_fear_val = dataframe_fear[int(0.6 * min_size):int(0.8 * min_size)]
    dataframe_fear_test = dataframe_fear[int(0.8 * min_size):]
    dataframe_sadness_train = dataframe_sadness[:int(0.8 * min_size)]
#    dataframe_sadness_val = dataframe_sadness[int(0.6 * min_size):int(0.8 * min_size)]
    dataframe_sadness_test = dataframe_sadness[int(0.8 * min_size):]
    dataframe_happiness_train = dataframe_happiness[:int(0.8 * min_size)]
#    dataframe_happiness_val = dataframe_happiness[int(0.6 * min_size):int(0.8 * min_size)]
    dataframe_happiness_test = dataframe_happiness[int(0.8 * min_size):]
    dataframe_train = pd.concat([dataframe_anger_train, dataframe_fear_train, dataframe_sadness_train,
                                 dataframe_happiness_train]).sample(frac=1).reset_index(drop=True)
#    dataframe_val = pd.concat([dataframe_anger_val, dataframe_fear_val, dataframe_sadness_val,                               dataframe_happiness_val]).sample(frac=1).reset_index(drop=True)
    dataframe_test = pd.concat([dataframe_anger_test, dataframe_fear_test, dataframe_sadness_test,
                                dataframe_happiness_test]).sample(frac=1).reset_index(drop=True)
    train_list = [dataframe_train["text"], dataframe_train["emotion"]]
#    val_list = [dataframe_val["text"], dataframe_val["emotion"]]
    test_list = [dataframe_test["text"], dataframe_test["emotion"]]
    return train_list,  test_list, labels_dict
def load_tec_multiclass():

    # Loads the TEC dataset for multiclass classification with the SASE model.

    labels_dict = {0: "Anger", 1: "Fear", 2: "Sadness", 3: "Happiness"}
    dataframe = pd.read_csv(base_path+"TEC.csv", delimiter=",", dtype={"id": int, "text": str, "emotion": str})
    dataframe = dataframe.drop(dataframe.columns[0], axis=1)
    dataframe = dataframe[dataframe.emotion != "disgust"]
    dataframe = dataframe[dataframe.emotion != "surprise"]
    dataframe["emotion"] = dataframe["emotion"].replace("anger", 0).replace("fear", 1)
    dataframe["emotion"] = dataframe["emotion"].replace("sadness", 2).replace("joy", 3)
    dataframe = dataframe.sample(frac=1).reset_index(drop=True)
    min_size = 1550
    dataframe_anger = dataframe[dataframe.emotion == 0][:min_size]
    dataframe_fear = dataframe[dataframe.emotion == 1][:min_size]
    dataframe_sadness = dataframe[dataframe.emotion == 2][:min_size]
    dataframe_happiness = dataframe[dataframe.emotion == 3][:min_size]
    dataframe_anger_train = dataframe_anger[:int(0.8 * min_size)]
    dataframe_anger_test = dataframe_anger[int(0.8 * min_size):]
    dataframe_fear_train = dataframe_fear[:int(0.8 * min_size)]
    dataframe_fear_test = dataframe_fear[int(0.8 * min_size):]
    dataframe_sadness_train = dataframe_sadness[:int(0.8 * min_size)]
    dataframe_sadness_test = dataframe_sadness[int(0.8 * min_size):]
    dataframe_happiness_train = dataframe_happiness[:int(0.8 * min_size)]
    dataframe_happiness_test = dataframe_happiness[int(0.8 * min_size):]
    dataframe_train = pd.concat([dataframe_anger_train, dataframe_fear_train, dataframe_sadness_train,
                                 dataframe_happiness_train]).sample(frac=1).reset_index(drop=True)
    dataframe_test = pd.concat([dataframe_anger_test, dataframe_fear_test, dataframe_sadness_test,
                                dataframe_happiness_test]).sample(frac=1).reset_index(drop=True)
    train_list = [dataframe_train["text"], dataframe_train["emotion"]]
    test_list = [dataframe_test["text"], dataframe_test["emotion"]]
    return train_list, test_list, labels_dict


def load_wang_multiclass():

    # Loads the Wang dataset for multiclass classification with the SASE model.

    labels_dict = {0: "Anger", 1: "Fear", 2: "Sadness", 3: "Happiness"}
    dataframe = pd.read_csv(base_path+"Wang.csv", delimiter=",", dtype={"id": int, "text": str, "emotion": str})
    dataframe = dataframe.drop(dataframe.columns[0], axis=1)
    dataframe = dataframe[dataframe.emotion != "love"]
    dataframe = dataframe[dataframe.emotion != "thankfulness"]
    dataframe = dataframe[dataframe.emotion != "surprise"]
    dataframe["emotion"] = dataframe["emotion"].replace("anger", 0).replace("fear", 1)
    dataframe["emotion"] = dataframe["emotion"].replace("sadness", 2).replace("joy", 3)
    dataframe = dataframe.sample(frac=1).reset_index(drop=True)
    min_size = 10000
    dataframe_anger = dataframe[dataframe.emotion == 0][:min_size]
    dataframe_fear = dataframe[dataframe.emotion == 1][:min_size]
    dataframe_sadness = dataframe[dataframe.emotion == 2][:min_size]
    dataframe_happiness = dataframe[dataframe.emotion == 3][:min_size]
    dataframe_anger_train = dataframe_anger[:int(0.8 * min_size)]
    dataframe_anger_test = dataframe_anger[int(0.8 * min_size):]
    dataframe_fear_train = dataframe_fear[:int(0.8 * min_size)]
    dataframe_fear_test = dataframe_fear[int(0.8 * min_size):]
    dataframe_sadness_train = dataframe_sadness[:int(0.8 * min_size)]
    dataframe_sadness_test = dataframe_sadness[int(0.8 * min_size):]
    dataframe_happiness_train = dataframe_happiness[:int(0.8 * min_size)]
    dataframe_happiness_test = dataframe_happiness[int(0.8 * min_size):]
    dataframe_train = pd.concat([dataframe_anger_train, dataframe_fear_train, dataframe_sadness_train,
                                 dataframe_happiness_train]).sample(frac=1).reset_index(drop=True)
    dataframe_test = pd.concat([dataframe_anger_test, dataframe_fear_test, dataframe_sadness_test,
                                dataframe_happiness_test]).sample(frac=1).reset_index(drop=True)
    train_list = [dataframe_train["text"], dataframe_train["emotion"]]
    test_list = [dataframe_test["text"], dataframe_test["emotion"]]
    return train_list, test_list, labels_dict



def load_embedding(path, word_to_id, embedding_dimension):

    # Loads the pretrained embedding from a file.

    # Parameters:
    #     path: {str} Path to the pretrained embedding file.
    #     word_to_id: {dict} Vocabulary dictionary.
    #     embedding_dimension: {int} Embedding dimension.

    vocabulary_size = len(word_to_id) + 1
    with open(path) as f:
        embedding = np.zeros((vocabulary_size, embedding_dimension))
        for line in f.readlines():
            values = line.split()
            word = values[0]
            index = word_to_id.get(word)
            if index:
                vector = np.array(values[1:], dtype='float32')
                if vector.shape[-1] != embedding_dimension:
                    raise Exception("Dimensions do not match!")
                embedding[index] = vector
    return torch.from_numpy(embedding).float()



def load_embedding_bigru(path, word_to_id, max_features, embedding_dimension):

    # Loads the pretrained embedding from a file.

    # Parameters:
    #     path: {str} Path to the pretrained embedding file.
    #     word_to_id: {dict} Vocabulary dictionary.
    #     max_features: {int} Maximal size of the vocabulary.
    #     embedding_dimension: {int} Embedding dimension.

    vocabulary_size = len(word_to_id) + 1
    num_words = vocabulary_size#  min(max_features, vocabulary_size)
    with open(path) as f:
        embedding = np.zeros((num_words, embedding_dimension))
        for line in f.readlines():
            values = line.split()
            word = values[0]
            index = word_to_id.get(word)
            if index:# and index < num_words:
                vector = np.array(values[1:], dtype='float32')
                if vector.shape[-1] != embedding_dimension:
                    raise Exception("Dimensions do not match!")
                embedding[index] = vector
    return embedding, num_words

def tokenize_map_pad_mask(sentences,MAX_LEN = 35):
  tokenizer = BertTokenizer.from_pretrained(base_path+'bert/', do_lower_case=True)
  # Tokenize all of the sentences and map the tokens to thier word IDs.
  input_ids = []
  # For every sentence...
  for sent in sentences:
    # `encode` will:
    #   (1) Tokenize the sentence.
    #   (2) Prepend the `[CLS]` token to the start.
    #   (3) Append the `[SEP]` token to the end.
    #   (4) Map tokens to their IDs.
    encoded_sent = tokenizer.encode(
                        sent,                      # Sentence to encode.
                        add_special_tokens = True, # Add '[CLS]' and '[SEP]'

                        # This function also supports truncation and conversion
                        # to pytorch tensors, but we need to do padding, so we
                        # can't use these features :( .
                        #max_length = 128,          # Truncate all sentences.
                        #return_tensors = 'pt',     # Return pytorch tensors.
                  )

    # Add the encoded sentence to the list.
    input_ids.append(encoded_sent)
    #pad the sentences
  input_ids = pad_sequences(input_ids, maxlen=MAX_LEN, dtype="long",
                        value=0, truncating="post", padding="post")


  # Create attention masks
  attention_masks = []
  # For each sentence...
  for sent in input_ids:
      # Create the attention mask.
      #   - If a token ID is 0, then it's padding, set the mask to 0.
      #   - If a token ID is > 0, then it's a real token, set the mask to 1.
      att_mask = [int(token_id > 0) for token_id in sent]

      # Store the attention mask for this sentence.
      attention_masks.append(att_mask)
  return input_ids,attention_masks

def train(model, train_loader, criterion, optimizer, num_epochs, gradient_clipping, verbose=True):

    # Trains the SASE model. Returns the accuracies and losses of the model after each epoch.

    # Parameters:
    #     model: {SelfAttentiveSentenceEmbeddingModel} SASE model to be trained.
    #     train_loader: {torch.utils.data.DataLoader} Data loader containing training data.
    #     optimizer: {torch.optim.RMSprop} Optimizer to be used in training.
    #     criterion: {torch.nn.BCELoss / torch.nn.NLLoss} Loss function to be used in training.
    #                Must be BCELoss for binary classification and NLLLoss for multiclass classification.
    #     num_epochs: {int} Number of epochs.
    #     penalization: {bool} Use penalization or not.
    #     penal_coefficient: {int} Penalization coefficient.
    #     gradient_clipping: {bool} Use gradient clipping or not.
    #     verbose: {bool} Verbose or not.

    if torch.cuda.is_available():
      device = torch.device("cuda")
    else:
      device = torch.device("cpu")

    losses = []
    accuracies = []
    for i in range(num_epochs):
        if verbose:
            print("Running epoch: " + str(i + 1))
        total_loss = 0
        num_batches = 0
        correct = 0
        for batch_id, train_data in enumerate(train_loader):

            output = train_data[1].to(device)
            input_data = train_data[0].to(device)
            input_ids = train_data[2].to(device)
            input_mask = train_data[3].to(device)


            predicted_output = model(input_data,input_ids,input_mask)##############################################################################
            loss = criterion(predicted_output, output)
            correct += torch.eq(torch.max(predicted_output, 1)[1], output.type(torch.LongTensor).to(device)).data.sum()

            total_loss += loss.item()
            optimizer.zero_grad()
            loss.backward()
            if gradient_clipping:
                torch.nn.utils.clip_grad_norm_(model.parameters(), 0.5)
            optimizer.step()
            num_batches += 1
            #print('after loss back')
        if verbose:
            print("Average loss: %0.3f" % (total_loss / num_batches))
        correct_np = correct.data.cpu().numpy().astype(int)
        accuracy = correct_np / float(num_batches * train_loader.batch_size)
        if verbose:
            print("Accuracy: %0.3f" % (accuracy))
        losses.append(total_loss / num_batches)
        accuracies.append(accuracy)
    return losses, accuracies

def evaluate(model, test_data, bert_test_data, bert_test_mask, test_output, verbose=True):

    # Evaluates the SASE model using test data and compares the predicted output with the test output.

    # Parameters:
    #     model: {SelfAttentiveSentenceEmbeddingModel} SASE model to be evaluated.
    #     test_data: {numpy.array} Test data.
    #     test_output: {numpy.array} Test output.
    #     verbose: {bool} Verbose or not.
    #device=torch.device("cpu")
    if torch.cuda.is_available():
      device = torch.device("cuda")
    else:
      device = torch.device("cpu")

    model.eval()
    model.set_hidden_state(test_data.shape[0])
    test_data_var = Variable(torch.from_numpy(test_data).type(torch.LongTensor)).to(device)
    bert_test_data = Variable(torch.from_numpy(bert_test_data).type(torch.LongTensor)).to(device)
    bert_test_mask = Variable(torch.from_numpy(bert_test_mask).type(torch.LongTensor)).to(device)

    predicted_test_output  = model(test_data_var, bert_test_data, bert_test_mask)

    predicted_test_output = torch.max(predicted_test_output, 1)[1]
    test_output_var = Variable(torch.from_numpy(test_output).type(torch.LongTensor)).to(device)
    correct = torch.eq(predicted_test_output, test_output_var).data.sum()
    correct_np = correct.data.cpu().numpy().astype(int)
    accuracy = correct_np / float(test_data_var.size(0))
    if verbose:
        print("Accuracy: %0.3f" % (accuracy))
    return accuracy, predicted_test_output

def multiclass_classification(model, train_loader, num_epochs, gradient_clipping, verbose=True):

    # Trains the SASE model for multiclass classification.

    # Parameters:
    #     model: {SelfAttentiveSentenceEmbeddingModel} SASE model to be trained.
    #     train_loader: {torch.utils.data.DataLoader} Data loader containing training data.
    #     num_epochs: {int} Number of epochs.
    #     penalization: {bool} Use penalization or not.
    #     penal_coefficient: {int} Penalization coefficient.
    #     gradient_clipping: {bool} Use gradient clipping or not.
    #     verbose: {bool} Verbose or not.

    criterion = torch.nn.NLLLoss()
    optimizer = torch.optim.Adam(model.parameters(),lr=.001)
    losses, accuracies = train(model, train_loader, criterion, optimizer, num_epochs,
                               gradient_clipping, verbose)
    return losses, accuracies





def run_multiclass_model(model_name, model_parameters, train_parameters, load_function, verbose=True):

    # Trains and tests the SASE model on multiclass classification.

    # Parameters:
    #     model_name: {str} Name of the model.
    #     model_parameters: {dict} Dictionary of model parameters.
    #     train_parameters: {dict} Dictionary of training parameters.
    #     load_function: {function} Function to load a dataset.
    #     verbose: {bool} Verbose or not.

    train_data, test_input, bert_test_input_np, bert_test_masks_np, test_output, word_to_id, labels_dict = load_dataset(load_function,
                                                                                model_parameters["max_features"],
                                                                                model_parameters["max_length"],
                                                                              model_parameters['batch_size'] )
    if torch.cuda.is_available():
      device = torch.device("cuda")
    else:
      device = torch.device("cpu")
    #sase model loading
    pretrained_embedding_sase = load_embedding(base_path+"glove.6B.50d.txt", word_to_id, model_parameters["embedding_dimension"])
    sase = torch.load(model_parameters['sase_path'], map_location = torch.device('cuda'))
    sase.batch_size = model_parameters['batch_size']
    sase.hidden_state = sase.init_hidden()
    sase.embedding, sase.embedding_dimension = sase.load_embedding(pretrained_embedding_sase)
    sase.eval()

    #bigru model loading
    pretrained_embedding_bigru = load_embedding(base_path+"glove.6B.300d.txt", word_to_id, 300)
    bigru = torch.load(model_parameters['bigru_path'],map_location = torch.device('cuda'))
    bigru.batch_size = model_parameters['batch_size']
    bigru.hidden_state = bigru.init_hidden()
    bigru.embedding, bigru.embedding_dimension = bigru.load_embedding(pretrained_embedding_bigru)
    bigru.eval()

    for param in sase.parameters():
      param.requires_grad = False
    for param in bigru.parameters():
      param.requires_grad = False

    bigru.to(device)
    sase.to(device)
    #sase=0

    model = dme(model_parameters['num_classes'], bigru, sase, model_parameters['projection_dim'])
    model.to(device)
    if verbose:
        print("TRAINING:")

    losses, accuracies = multiclass_classification(model, train_data, train_parameters["num_epochs"],
                                                   train_parameters["gradient_clipping"], verbose)
    if verbose:
        print("POST-TRAINING:")
    post_accuracy, post_predicted_test_output = evaluate(model, test_input, bert_test_input_np, bert_test_masks_np, test_output, verbose)
    if verbose:
        print("EVALUATION REPORT:")
    result = evaluation_summary(model_name, post_predicted_test_output.detach().numpy(), test_output, labels_dict, verbose)
    if verbose:
        plot_confusion_matrix(model_name, post_predicted_test_output.detach().numpy(), test_output, labels_dict)
        plot_confusion_matrix(model_name, post_predicted_test_output.detach().numpy(), test_output, labels_dict, True)
    return result, model
print('functions and classes defined')

train_parameters = {
    "num_epochs": 5,
	  "penalization": True,
	  "penal_coefficient": 0.03,
	  "gradient_clipping": True
}

model_parameters = {
    'projection_dim':80,
    'sase_path':sase_path_cf,
    'bigru_path':bigru_path_cf,
    "batch_size": 16,
    "lstm_hidden_dimension": 500,
    "dense_hidden_dimension": 100,
    "num_attention_hops": 10,
    "num_timesteps": 35,
    "embedding_dimension": 50,
    "vocabulary_size": 20000,
    "num_classes": 4,
    "max_features": 25000,
    "max_length": 35
}
print('execution loading....')

#you can change the dataset to train and test by changing the last parameter of the above line of code
#to load_tec_multiclass or load_wang_multiclass
result , model = run_multiclass_model("CF(MULTICLASS)", model_parameters, train_parameters, load_crowdflower_multiclass)
parameters = ["CF(MULTICLASS)", model_parameters, train_parameters, load_crowdflower_multiclass]
create_report(parameters, run_multiclass_model, parameters[1]["num_classes"], 4)
