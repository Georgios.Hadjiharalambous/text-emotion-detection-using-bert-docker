FROM python
RUN pip install torch==1.3.1+cu92 -f https://download.pytorch.org/whl/torch_stable.html 
RUN pip install numpy
RUN pip install pandas
RUN pip install matplotlib
RUN pip install sklearn
RUN pip install keras
RUN pip install tensorflow
RUN pip install transformers


